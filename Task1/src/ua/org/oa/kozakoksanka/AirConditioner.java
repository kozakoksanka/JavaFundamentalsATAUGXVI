package ua.org.oa.kozakoksanka;

/**
 * Represents air conditioner instance.
 *
 * @author Oksana Zaika
 */
public class AirConditioner {
    /**
     * Conditioner name
     */
    private String name;
    /**
     * Conditioner age
     */
    private int age;
    /**
     * Contain value of breakdowns during last year
     */
    private int breakdownAmount;
    /**
     * True if conditioner working now, otherwise false
     */
    private boolean isWorkingNow;
    /**
     * True if conditioner was ever repair, otherwise false
     */
    private boolean isEverRepair;

    /**
     * Constructor
     *
     * @param name            Conditioner name
     * @param age             Conditioner age
     * @param breakdownAmount Amount of conditioner break downs
     * @param isWorkingNow    True if conditioner working now, otherwise false.
     * @param isEverRepair    True if conditioner was ever repaired, otherwise false.
     */
    public AirConditioner(String name, int age, int breakdownAmount, boolean isWorkingNow, boolean isEverRepair) {
        this.name = name;
        this.age = age;
        this.breakdownAmount = breakdownAmount;
        this.isWorkingNow = isWorkingNow;
        this.isEverRepair = isEverRepair;
    }

    /** Create a method that determines the need for maintenance*/
    public void obtainInformationAboutService() {
        if (age < 2 && breakdownAmount == 0 && isWorkingNow && !isEverRepair) {
            System.out.println("Ваш кондиционер " + name + " работает исправно, значит необходимости в ТО нет.");
        } else if (age < 5 && breakdownAmount <= 5 && isWorkingNow && isEverRepair) {
            System.out.println("Ваш кондиционер " + name + " уже не новый, но работает вполне стабильно. " +
                    "Рекомендуем обратится в сервисную службу для определения причин " +
                    "несистемных ошибок и для проведения планового ТО.");
        } else if (age > 5 && breakdownAmount <= 10 && !isWorkingNow && !isEverRepair) {
            System.out.println("Вам необходимо обратится в сервисную службу для определения причин " +
                    "неработоспособности вашего кондиционера " + name + " и устранения возможных причин.");
        } else {
            System.out.println("Ваш кондиционер " + name + " относится к безнадежным случаям, " +
                    "ему уже никто не поможет.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Name:" + name + "\n" +
                "Age:" + age + "\n" +
                "Ever repaired:" + (isEverRepair ? "Yes" : "No") + "\n" +
                "Breakdowns:" + breakdownAmount + "\n" +
                "Is working now:" + (isWorkingNow ? "Yes" : "No");
    }

    /**
     * Prints all necessary information about conditioner
     */
    public void printConditionerInfo() {
        System.out.println(toString());
        System.out.print("Service info: ");
        obtainInformationAboutService();
        // Empty string divider
        System.out.println();
    }
}
