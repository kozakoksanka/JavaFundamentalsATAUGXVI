package ua.org.oa.kozakoksanka;

/**
 * Application main class
 *
 * @author Oksana Zaika
 */
public class InformationAboutConditioner {
    /**
     * Application entry point method
     *
     * @param arguments Console arguments
     */
    public static void main(String[] arguments) {
        AirConditioner conditionerToshiba = new AirConditioner("Toshiba", 1, 0, true, false);
        conditionerToshiba.printConditionerInfo();

        AirConditioner conditionerLG = new AirConditioner("LG", 4, 5, true, true);
        conditionerLG.printConditionerInfo();

        AirConditioner conditionerElectrolux = new AirConditioner("Electrolux", 7, 9, false, false);
        conditionerElectrolux.printConditionerInfo();

        AirConditioner conditionerVeryOld = new AirConditioner("Zanussi", 25, 5, false, true);
        conditionerVeryOld.printConditionerInfo();
    }
}


