/**
 * @author Oksana Zaika.
 */
public class Utils {

    // 1 Task
    public long getSquare(int side) {
        return side * side;
    }

    // 2 Task
    public long calculateDistance(int firstCarSpeed, int secondCarSpeed, int distanceBetweenThem, int time) {
        return Math.abs(distanceBetweenThem - ((firstCarSpeed + secondCarSpeed) * time));
    }

    // 3 Task
    public void calculateRoots(int a, int b, int c) {
        int d = b * b - 4 * a * c;
        double x1 = (-b + Math.sqrt(d)) / (2 * a);
        double x2 = (-b - Math.sqrt(d)) / (2 * a);
        System.out.println(x1 + " " + x2);

    }

    // 4 Task
    public int camperNum(int a) {
        int result = 0;
        if (a > 0) {
            result = a += 1;
        } else if (a < 0) {
            result = a -= 2;
        } else if (a == 0) {
            result = a = 10;
        }
        return result;
    }


    // 5 Task
    public void printSumOfTwoMaxNumbers(int[] nums) {
        int maxOne = 0;
        int maxTwo = 0;
        for (int n : nums) {
            if (maxOne < n) {
                maxTwo = maxOne;
                maxOne = n;
            } else if (maxTwo < n) {
                maxTwo = n;
            }
        }
        int sumOfLargestNumbers = maxOne + maxTwo;
        System.out.println("Sum Of Largest Numbers is: " + sumOfLargestNumbers);
    }

    // 6 Task
    public void printDescriptionsOfNumber(int a) {
        if (a == 0) {
            System.out.println("нулевое число");
        }
        if (a > 0) {
            if (a % 2 == 0)
                System.out.println("положительное четное число");
        }
        if (a > 0) {
            if (a % 2 == 1) {
                System.out.println("положительное нечетное число");
            }
        }
        if (a < 0) {
            if (a % 2 == 0) {
                System.out.println("отрицательное четное число");
            }
        }
        if (a < 0) {
            if (a % 2 == -1) {
                System.out.println("отрицательное нечетное число");
            }
        }
    }

    // 7 Task
    public void getTrueOrFalseOfAB(int a, int b) {
        System.out.println(a > 2);
        System.out.println(b <= 3);
    }

    // 8 Task
    public String getBoolResultOfABC(int a, int b, int c) {
        return (a < b && b < c) ? "true" : "false";
    }

    // 9 Task
    public void printNumber(int a, int b, int c, int d) {
        if (a == b && a == c) {
            System.out.println("4");
        } else if (b == a && b == d) {
            System.out.println("3");
        } else if (c == a && c == d) {
            System.out.println("2");
        } else if (d == b && d == c) {
            System.out.println("1");
        }
    }

    // 10 Task
    public void printValue(int k) {
        switch (k) {
            case 1:
                System.out.println("1 - это плохо");
                break;
            case 2:
                System.out.println("2 - это неудовлетворительно");
                break;
            case 3:
                System.out.println("3 - это удовлетворительно");
                break;
            case 4:
                System.out.println("4 - это хорошо");
                break;
            case 5:
                System.out.println("5 - это отлично");
                break;
            default:
                System.out.println("Ошибка");
                break;
        }
    }

    // 11 Task
    public void printTimeOfYear(int month) {
        if (month == 12 || month <= 2) {
            System.out.println("Этот месяц относится к зиме");
        } else if (month >= 3 && month <= 5) {
            System.out.println("Этот месяц относится к весне");
        } else if (month >= 6 && month <= 8) {
            System.out.println("Этот месяц относится к лету");
        } else if (month >= 9 && month <= 11) {
            System.out.println("Этот месяц относится к осени");
        } else if (month <= 0 || month > 12) {
            System.out.println("Такого месяца не существует");
        }
    }

    // 12 Task
    public int doingOperationAndGetResult(int numOfOperation, int a, int b) {
        int result = 0;
        if (b != 0) {
            switch (numOfOperation) {
                case 1:
                    result = a + b;
                    break;
                case 2:
                    result = a - b;
                    break;
                case 3:
                    result = a * b;
                    break;
                case 4:
                    result = a / b;
                    break;
                default:
                    System.out.println("Wrong operation number!");
            }
        } else {
            System.out.println("Argument b must not be equal 0!");
        }
        return result;
    }


    // 13 Task
    public void printIncrementNum(int start, int end) {
        int num = 1;
        int counter = 0;
        for (int i = start; i <= end; i++, num++) {
            counter++;
            System.out.println("Число в порядке возрастания: " + i);
        }
        System.out.println("Всего чисел " + counter);
    }

    // 14 Task
    public int printSumOfIncrementNum(int start, int end) {
        int sum = 0;
        for (int i = start; i <= end; i++) {
            sum = sum + i;
        }
        return sum;
    }

    // 15 Task
    public int printFactorial(int n) {
        int factorial = 1;
        for (int i = 1; i <= n; i++) {
            factorial = factorial * i;
        }
        return factorial;
    }

    // 16 Task
    public int printFreeArea(int bigLength, int smallLength) {
        while ((bigLength - smallLength) >= 0) {
            bigLength -= smallLength;
        }
        return bigLength;
    }

    // 17 Task
    public int getSmallestValueOfK(int n) {
        int k = 1;
        while ((3 * k) < n) {
            k++;
        }
        return k;
    }

    // 18 Task
    public void getRightNumber(int num) {
        while (num != 0) {
            System.out.println(num % 10); // Печатаем крайнюю правую цифру числа `num`
            num = num / 10; // Изменяем переменную `num` таким образом, чтобы она стала равна числу из оставшихся цифр
        }
    }

    // 19 Task
    public void printFromAtoBPlus(int a, int b) {
        int i, i2;
        for (i = 1; i <= b - a + 1; ++i) {
            for (i2 = 1; i2 <= i; ++i2)
                System.out.println(a + i - 1);

        }
    }
}









