/**
 * @author Oksana Zaika.
 */
public class Test {
    public static void main(String[] arguments) {
        Utils myUtils = new Utils();
// Task 1
        System.out.println("-------------------Test 1-------------------");
        System.out.println(myUtils.getSquare(10));

// Task 2
        System.out.println("-------------------Test 2-------------------");
        System.out.println(myUtils.calculateDistance(75, 100, 1000, 2));

// Task 3
        System.out.println("-------------------Test 3-------------------");
        myUtils.calculateRoots(1, 2, -5);

        // Task 4
        System.out.println("-------------------Test 4-------------------");
        System.out.println(myUtils.camperNum(6));


// Task 5
        System.out.println("-------------------Test 5-------------------");
        int num[] = {8, 5, 10};
        myUtils.printSumOfTwoMaxNumbers(num);

//6 Task
        System.out.println("-------------------Test 6-------------------");
        myUtils.printDescriptionsOfNumber(0);

// 7 Task
        System.out.println("-------------------Test 7-------------------");
        myUtils.getTrueOrFalseOfAB(0, 3);

// 8 Task
        System.out.println("-------------------Test 8-------------------");
        System.out.println(myUtils.getBoolResultOfABC(10, 6, 8));

// 9 Task
        System.out.println("-------------------Test 9-------------------");
        myUtils.printNumber(1, 1, 6, 1);


// 10 Task
        System.out.println("-------------------Test 10-------------------");
        myUtils.printValue(5);


// 11 Task
        System.out.println("-------------------Test 11-------------------");
        myUtils.printTimeOfYear(6);

// 12 Task
        System.out.println("-------------------Test 12-------------------");
        System.out.println(myUtils.doingOperationAndGetResult(3, 10, 30));

// 13 Task
        System.out.println("-------------------Test 13-------------------");
        myUtils.printIncrementNum(2, 6);

// 14 Task
        System.out.println("-------------------Test 14------------------");
        System.out.println(myUtils.printSumOfIncrementNum(5, 10));

// 15 Task
        System.out.println("-------------------Test 15------------------");
        System.out.println(myUtils.printFactorial(5));

// 16 Task
        System.out.println("-------------------Test 16------------------");
        System.out.println(myUtils.printFreeArea(22, 5));


// 17 Task
        System.out.println("-------------------Test 17------------------");
        System.out.println(myUtils.getSmallestValueOfK(7));


// 18 Task
        System.out.println("-------------------Test 18------------------");
        myUtils.getRightNumber(1963254);

// 19 Task
        System.out.println("-------------------Test 19------------------");
        myUtils.printFromAtoBPlus(3, 6);
    }
}
