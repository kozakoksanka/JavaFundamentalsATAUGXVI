package ua.org.oa.kozakoksanka;

import java.util.ArrayList;
import java.util.List;

/**
 * Test class.
 *
 * @author Oksana Zaika.
 */
public class TestClass {

    /**
     * Application entry point method.
     *
     * @param args Console arguments.
     */
    public static void main(String[] args) {

        // Create list.
        List<Vehicle> cars = new ArrayList<>();

        // Add objects into the list.
        cars.add(new Car("BMW", "X3", "hatchback-B", 06, 79000));
        cars.add(new Car("BMW", "X2", "hatchback-B", 150, 82500));
        cars.add(new Car("BMW", "X1", "hatchback-B", 85, 65000));
        cars.add(new RallyAuto("Ford", "Mustang", "coupe", 320, 300.0));
        cars.add(new RallyAuto("Макларен", "М23", "coupe", 320, 326.0));
        cars.add(new RallyAuto("Феррари", " 500", "coupe", 320, 363.0));
        cars.add(new Truck("Урал", "4520", "грузовой", 90, 4000.0));
        cars.add(new Truck("МАЗ", "6530", "грузовой", 105, 3500.0));
        cars.add(new Truck("КРАЗ", "132", "грузовой", 96, 4200.0));

        for (Vehicle car : cars) {
            if (car instanceof Car) {
                ((Car) car).printCarWithLowerPrice(70000);
            } else if (car instanceof RallyAuto) {
                ((RallyAuto) car).printCarWithAboveSpeed(350);
            } else {
                ((Truck) car).printTruckInDiapasonCarryingCapacity(3700.0, 4000.0);
            }
        }
    }






}








