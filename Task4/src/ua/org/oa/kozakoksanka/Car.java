package ua.org.oa.kozakoksanka;

/**
 * Represents car and provides a method to print all of cars the price is lower than indicated price.
 *
 * @author Oksana Zaika.
 */
public class Car extends Vehicle {

    /**
     * Price of a car.
     */
    private int price;

    /**
     * Constructor
     *
     * @param brand         Name of car's brand.
     * @param model         Name of car's model.
     * @param typeOfVehicle Name of vehicle's type.
     * @param powerOfAuto   Value of cars's power (horse power).
     * @param price         Price of a car.
     */
    public Car(String brand, String model, String typeOfVehicle, int powerOfAuto, int price) {
        super(brand, model, typeOfVehicle, powerOfAuto);
        setPrice(price);
    }

    /**
     * @return car's price.
     */
    public int getPrice() {
        return price;
    }

    /**
     * Sets car's price.
     *
     * @param price Price of a car.
     */
    public void setPrice(int price) {
        if (price > 0) {
            this.price = price;
        } else {
            System.out.println("Цена не может быть отрицательной");
        }

    }

    /**
     * Prints car if its price lower than indicated.
     *
     * @param indicatedPrice Indicated price for car fetching.
     */
    public void printCarWithLowerPrice(int indicatedPrice) {
        if (getPrice() < indicatedPrice) {
            System.out.println("Информация о легковой машине, цена которой ниже " +
                    indicatedPrice + " y.e.: " + toString());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return super.toString() + ", цена: " + getPrice() + " у.е.";
    }
}

