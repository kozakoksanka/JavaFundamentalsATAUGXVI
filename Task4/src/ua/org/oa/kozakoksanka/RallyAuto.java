package ua.org.oa.kozakoksanka;

/**
 * Represents rally cars and provides a method which print all rally cars with speed is above of indicanted speed.
 *
 * @author Oksana Zaika.
 */
public class RallyAuto extends Vehicle {
    /**
     * Speed of rally car.
     */
    private double speed;

    /**
     * Constructor
     *
     * @param brand         Name of rally's car brand.
     * @param model         Name of auto's model.
     * @param typeOfVehicle Name of vehicle's type.
     * @param powerOfAuto   Value of auto's power (horse power).
     * @param speed         Value of auto's speed.
     */
    public RallyAuto(String brand, String model, String typeOfVehicle, int powerOfAuto, double speed) {
        super(brand, model, typeOfVehicle, powerOfAuto);
        setSpeed(speed);
    }

    /**
     * @return value of rally car's speed.
     */
    public double getSpeed() {
        return speed;
    }

    /**
     * Sets value of rally car's speed.
     *
     * @param speed Value of rally car's speed.
     */
    public void setSpeed(double speed) {
        if (speed > 0) {
            this.speed = speed;
        } else {
            System.out.println("Скорость не может быть отрицательной.");
        }
    }

    /**
     * Prints rally car if its speed is above of indicated speed.
     *
     * @param indicatedSpeed Value of indicated speed.
     */
    public void printCarWithAboveSpeed(double indicatedSpeed) {
        if (getSpeed() > indicatedSpeed) {
            System.out.println(("Информация о гоночной машине, скорость которой выше "
                    + indicatedSpeed + " км/ч: " + toString()));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return super.toString() + "максимальная скорость: " + getSpeed() + " км/ч.";
    }
}

