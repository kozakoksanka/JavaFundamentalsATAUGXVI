package ua.org.oa.kozakoksanka;

/**
 * Represents trucks and provides a method to print all of trucks if their carrying capacity
 * is located in indicated diapason.
 *
 * @author Oksana Zaika.
 */
public class Truck extends Vehicle {
    /**
     * Carrying capacity of truck.
     */
    private double carryingCapacity;

    /**
     * Constructor
     *
     * @param brand            Name of car's brand.
     * @param model            Name of car's model.
     * @param typeOfVehicle    Name of vehicle's type.
     * @param powerOfAuto      Value of cars's power (horse power).
     * @param carryingCapacity Carrying capacity of truck.
     */
    public Truck(String brand, String model, String typeOfVehicle, int powerOfAuto, double carryingCapacity) {
        super(brand, model, typeOfVehicle, powerOfAuto);
        setCarryingCapacity(carryingCapacity);
    }

    /**
     * @return value of carrying capacity of truck.
     */
    public double getCarryingCapacity() {
        return carryingCapacity;
    }

    /**
     * Sets value of carrying capacity of truck.
     *
     * @param carryingCapacity Value of carrying capacity of truck.
     */
    public void setCarryingCapacity(double carryingCapacity) {
        if (carryingCapacity > 0) {
            this.carryingCapacity = carryingCapacity;
        } else {
            System.out.println("Значение грузоподъемности не может быть отрицательным");
        }
    }

    /**
     * Prints truck if its the carrying capacity is located in indicated diapason.
     *
     * @param from Value of minimal carrying capacity.
     * @param to Value of maximal carrying capacity.
     */
    public void printTruckInDiapasonCarryingCapacity(double from, double to) {
        if (getCarryingCapacity() >= from && getCarryingCapacity() <= to) {
            System.out.println("Информация о грузовой машине, с грузоподьемностью в диапазоне от " + from
                    + " до " + to + " кг: " + toString());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return super.toString() + "с грузоподьемностью: " + getCarryingCapacity() + " кг.";
    }
}
