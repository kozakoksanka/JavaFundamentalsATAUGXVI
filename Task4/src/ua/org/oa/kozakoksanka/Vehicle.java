package ua.org.oa.kozakoksanka;

/**
 * Represents a simple model of vehicle.
 *
 * @author Oksana Zaika.
 */
public class Vehicle {
    /**
     * Name of auto's brand.
     */
    private String brand;
    /**
     * Name of auto's model.
     */
    private String model;
    /**
     * Name of vehicle's type.
     */
    private String typeOfVehicle;
    /**
     * Value of auto's power (horse power)
     */
    private int powerOfAuto;

    /**
     * Constructor
     *
     * @param brand         Name of auto's brand.
     * @param model         Name of auto's model.
     * @param typeOfVehicle Name of vehicle's type.
     * @param powerOfAuto   Value of auto's power (horse power).
     */
    public Vehicle(String brand, String model, String typeOfVehicle, int powerOfAuto) {
        setBrand(brand);
        setModel(model);
        setTypeOfVehicle(typeOfVehicle);
        setPowerOfAuto(powerOfAuto);
    }

    /**
     * @return brand name.
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Sets brand name.
     *
     * @param brand Name of auto's brand.
     */
    public void setBrand(String brand) {
        if (brand != null) {
            this.brand = brand;
        } else {
            System.out.println("Введите название бренда авто.");
        }
    }

    /**
     * @return name of auto's model.
     */
    public String getModel() {
        return model;
    }

    /**
     * Sets name of auto's model.
     *
     * @param model name of auto's model.
     */

    public void setModel(String model) {
        if (model != null) {
            this.model = model;
        } else {
            System.out.println("Введите название модели авто.");
        }
    }

    /**
     * @return name of vehicle's type.
     */

    public String getTypeOfVehicle() {
        return typeOfVehicle;
    }

    /**
     * Sets Name of vehicle's type.
     *
     * @param typeOfVehicle name of vehicle's type.
     */
    public void setTypeOfVehicle(String typeOfVehicle) {
        if (typeOfVehicle != null) {
            this.typeOfVehicle = typeOfVehicle;
        } else {
            System.out.println("Укажите тип траспортного средства.");
        }
    }

    /**
     * @return value of auto's power (horse power).
     */
    public int getPowerOfAuto() {
        return powerOfAuto;
    }

    /**
     * Sets value of auto's power (horse power).
     *
     * @param powerOfAuto value of auto's power (horse power).
     */
    public void setPowerOfAuto(int powerOfAuto) {
        if (powerOfAuto > 0) {
            this.powerOfAuto = powerOfAuto;
        } else {
            System.out.println("Мощность авто не может быть отрицательной.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Информация о машине: " +
                "автомобиль марки: " + brand +
                ", модель: " + model +
                ", тип траспортного средства: " + typeOfVehicle +
                ", мощность: " + powerOfAuto + " л. с.";
    }
}
