package ua.org.oa.kozakoksanka;

import java.io.*;

/**
 * Represents methods which calculate amount of words with symbols duplicate.
 *
 * @author Oksana Zaika
 */
public class DuplicateSymbolAnalyzerVar2 {
    /**
     * Buffered reader.
     */
    private final BufferedReader bufferedReader;

    /**
     * Constructor
     *
     * @param fileName Path to file which need to read.
     * @throws FileNotFoundException trows exception in case if file not found.
     */
    public DuplicateSymbolAnalyzerVar2(String fileName) throws FileNotFoundException {
        FileReader reader = new FileReader(fileName);
        bufferedReader = new BufferedReader(reader);
    }

    /**
     * Method which reads text by lines and converts it into the String.
     *
     * @return return done string.
     * @throws IOException throws exception in case of input and output problems.
     */
    private String getText() throws IOException {
        String line;
        String text = "";
        while ((line = bufferedReader.readLine()) != null) {
            text += line;
        }
        return text;
    }

    /**
     * Second method which calculates duplicate symbols in given text.
     *
     * @param text Text for analyze.
     * @return amount of found duplicates.
     */

    private int countDuplicateSymbolsVar2(String text) {
        int result = 0;
        String string = new String(text);
        String[] textArray = string.split(" ");
        for (String s : textArray) {
            StringBuffer word = new StringBuffer(s);
            for (int i = 0; i < word.length() - 1; i++) {
                if (word.charAt(i) == word.charAt(i + 1)) {
                    result++;
                    break;
                }

            }
        }
        return result;
    }

    /**
     * Method which print amount of words with symbols duplicate.
     */

    public void printDuplicateAmountVar2() {
        try {
            System.out.println(countDuplicateSymbolsVar2(getText()));
        } catch (Exception exception) {
            System.out.println("Ошибка ввода-вывода 2");
        }
    }
}






