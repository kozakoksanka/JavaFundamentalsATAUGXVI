package ua.org.oa.kozakoksanka;

import java.io.IOException;

/**
 * Test class.
 *
 * @author Oksana Zaika
 */
public class TestClass {
    /**
     * Application entry point method.
     *
     * @param args console arguments.
     * @throws IOException throws exception in case of input and output problems.
     */
    public static void main(String[] args) throws IOException {

        // Create new object DuplicateSymbolAnalyzerVar1.
        DuplicateSymbolAnalyzerVar1 text = new DuplicateSymbolAnalyzerVar1("/home/oksana/Workspace/Education/JavaFundamentalsATAUGXVI/Task3/input1.txt");

        // test a printDuplicateAmount method.

        text.printDuplicateAmount();

        // Create new object DuplicateSymbolAnalyzerVar1.

        DuplicateSymbolAnalyzerVar2 text2 = new DuplicateSymbolAnalyzerVar2("/home/oksana/Workspace/Education/JavaFundamentalsATAUGXVI/Task3/input1.txt");

        // test a 2 printDuplicateAmount method.
        text2.printDuplicateAmountVar2();
    }
}



