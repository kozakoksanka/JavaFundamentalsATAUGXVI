package ua.org.oa.kozakoksanka;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Represents methods which calculate amount of words with duplicates symbols .
 *
 * @author Oksana Zaika
 */

public class DuplicateSymbolAnalyzerVar1 {
    /**
     * Buffered reader.
     */
    private final BufferedReader bufferedReader;

    /**
     * Constructor
     *
     * @param fileName Path to file which need to read.
     * @throws FileNotFoundException trows exception in case if file not found.
     */
    public DuplicateSymbolAnalyzerVar1(String fileName) throws FileNotFoundException {
        FileReader reader = new FileReader(fileName);
        bufferedReader = new BufferedReader(reader);
    }

    /**
     * Method which reads text by lines and converts it into the String.
     *
     * @return return done string.
     * @throws IOException throws exception in case of input and output problems.
     */
    private String getText() throws IOException {
        String line;
        String text = "";
        while ((line = bufferedReader.readLine()) != null) {
            text += line;
        }
        return text;
    }

    /**
     * First method which calculates duplicate symbols in given text.
     *
     * @param text Text for analyze.
     * @return amount of found duplicates.
     */
    private int countDuplicateSymbols(String text) {
        int result = 0;
        char l = ' ';
        StringBuffer stringBuffer = new StringBuffer(text);
        for (int i = 0; i < stringBuffer.length() - 1; i++) {
            if (stringBuffer.charAt(i) == stringBuffer.charAt(i + 1)) {
                result++;
                if (i < stringBuffer.length() - 1) {
                    i++;
                }
                while (stringBuffer.charAt(i) != l && i < stringBuffer.length() - 1) {
                    if (stringBuffer.charAt(i) == stringBuffer.charAt(i + 1)) {
                        break;
                    }
                    i++;
                }
            }
        }
        return result;
    }

    /**
     * Method which print amount of words with symbols duplicate.
     */
    public void printDuplicateAmount() {
        try {
            System.out.println(countDuplicateSymbols(getText()));
        } catch (Exception exception) {
            System.out.println("Ошибка ввода-вывода");
        }
    }
}

            







