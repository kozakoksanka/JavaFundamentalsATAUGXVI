/**
 * Test class.
 * @author Oksana Zaika.
 */
public class TestClass {
    /**
     * Application entry point method.
     * @param args console arguments.
     */
    public static void main(String[] args) {
        Methods allMethods = new Methods();
        System.out.println("----------Exception example a----------");
        allMethods.representsArrayIndexOutOfBoundsException();

        System.out.println("----------Exception example b----------");
        allMethods.representsIllegalArgumentException(20,0);

        System.out.println("----------Exception example c----------");
        allMethods.representsClassCastException();

        System.out.println("----------Exception example d----------");
        allMethods.representsStringIndexOutOfBoundException();

        System.out.println("----------Exception example e----------");
        allMethods.representsNullPointerException();

        System.out.println("----------Exception example g----------");
        allMethods.representsNumberFormatException("hello12");

        System.out.println("----------Exception example f----------");
       allMethods.representsStackOverflowError();

        System.out.println("----------Exception example e----------");
        allMethods.representsOutOfMemoryError();
    }
}
