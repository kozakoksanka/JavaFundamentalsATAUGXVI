import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Oksana Zaika.
 */
public class Methods {

    /**
     * Creates situation of emergence ArrayIndexOutOfBoundsException.
     * If delete symbol "=" in loop for, method will work right.
     */
    public void representsArrayIndexOutOfBoundsException() {
        int[] myArray = new int[2];
        int a = 10;
        try {
            for (int i = 0; i <= myArray.length; i++) {
                myArray[i] = a;
                a++;
            }
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Exception: " + e);
        }
        System.out.println("myArray consist of: " + Arrays.toString(myArray) + "\n");
    }

    /**
     * Represents work of IllegalArgumentException.
     * Divides numbers.
     *
     * @param a any number.
     * @param b any number.
     */
    public void representsIllegalArgumentException(int a, int b) {
        try {
            if ((a == 0) || (b == 0)) {
                throw new IllegalArgumentException("Arguments of method can not be 0.");
            } else {
                System.out.println("a / b = " + a / b);
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Exception: " + e + "\n");
        }
    }

    /**
     * Represents work of ClassCastException.
     */
    public void representsClassCastException() {
        Object string = "This is string.";
        try {
            int stringByNumber = (int) string;
            System.out.println(stringByNumber);
        } catch (ClassCastException e) {
            System.out.println("Exception: " + e + "\n");
        }
    }

    /**
     * Creates situation of emergence StringIndexOutOfBoundsException.
     * If delete symbol "=" in loop for, method will work right.
     */
    public void representsStringIndexOutOfBoundException() {
        String str = "Hello";
        try {
            for (int i = 0; i <= str.length(); i++)
                for (int j = i + 1; j <= str.length(); j++)
                    str.substring(i, j - i);
        } catch (StringIndexOutOfBoundsException e) {
            System.out.println("Exception: " + e + "\n");
        }
    }

    /**
     * Creates situations of emergence NullPointerException.
     *
     */
    public void representsNullPointerException (){
        // Var 1
        String [] myArray = new String[5];
        System.out.println("NPE 1");
        try {
            myArray[0].substring(1);
        } catch (NullPointerException e){
            System.out.println("Exception: " + e + "\n");
        }

        // Var 2
        System.out.println("NPE 2");
        Integer big = null;
        try {
            int small = big;
            System.out.println(small);
        } catch (NullPointerException e){
            System.out.println("Exception: " + e + "\n");
        }

        //Var 3
        System.out.println("NPE 3");
        Methods methods = null;
        try {
            methods.representsNullPointerException();
        } catch (NullPointerException e){
            System.out.println("Exception: " + e + "\n");
        }
    }

    /**
     * Creates situation of emergence StackOverflowError.
     */
    public void representsStackOverflowError() {
       representsStackOverflowError();
    }

    /**
     * Represents work of NumberFormatException.
     * @param str any string.
     */
    public void representsNumberFormatException(String str) {
        try {
            int int1 = Integer.parseInt(str);
            System.out.println(int1 + "\n");
        } catch (NumberFormatException e) {
            System.out.println("Exception: " + e + "\n");
        }
    }

    /**
     * Represents work of OutOfMemoryError.
     */
    public void representsOutOfMemoryError (){
        List <File> list = new ArrayList<>();
        for (int i = 1; i > 0; i++)
            list.add(new File ("/home/oksana/Загрузки/ideaIC-2016.2.tar.gz"));
        }
    }














