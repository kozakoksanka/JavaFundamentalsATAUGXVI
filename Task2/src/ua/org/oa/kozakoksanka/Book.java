package ua.org.oa.kozakoksanka;

/**
 * Simple model which represents book instance.
 *
 * @author Oksana Zaika
 */
public class Book {
    /**
     * Value of minimal price of each book in a library.
     */
    private static final int MIN_PRICE = 50;
    /**
     * Value of maximal price of each book in a library.
     */
    private static final int MAX_PRICE = 2000;
    /**
     * Book name.
     */
    private String name;
    /**
     * Name of books author.
     */
    private String author;
    /**
     * Value of books price.
     */
    private int price;
    /**
     * Number of edition.
     */
    private int edition;

    /**
     * Constructor
     *
     * @param name   Book name.
     * @param author Name of books author.
     * @param price  Value of books price.
     */
    public Book(String name, String author, int price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }

    /**
     * Constructor
     *
     * @param name    Book name.
     * @param author  Name of books author.
     * @param price   Value of books price.
     * @param edition Number of edition.
     */
    public Book(String name, String author, int price, int edition) {
        setName(name);
        setAuthor(author);
        setPrice(price);
        setEdition(edition);
    }

    /**
     * @return book name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets book name.
     *
     * @param name
     */
    public void setName(String name) {
        if (name != null) {
            this.name = name;
        } else {
            name = "";
            System.out.println("У книги должно быть название.");
        }
    }

    /**
     * @return name of books author.
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Sets name of books author.
     * @param author name of books author.
     */
    public void setAuthor(String author) {

        if (author != null) {
            this.author = author;
        } else {
            author = "" ;

            System.out.println("У книги должен быть автор.");
        }
    }

    /**
     * @return value of books price.
     */
    public int getPrice() {
        return price;
    }

    /**
     * Sets value of books price.
     * @param price value of books price.
     */
    public void setPrice(int price) {
        if (price > MIN_PRICE && price <= MAX_PRICE) {
            this.price = price;
        } else {
            System.out.println("Книг с такой ценой в библиотеке нет");
        }
    }

    /**
     * @return number of edition.
     */
    public int getEdition() {
        return edition;
    }

    /**
     * Sets number of edition.
     * @param edition number of edition.
     */
    public void setEdition(int edition) {
        if (edition >= 0) {
            this.edition = edition;
        } else {
            System.out.println("Номер издания не может иметь отрицательное значение");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        String editionValue;
        if (edition == 0) {
            editionValue = "1";
        } else
            editionValue = Integer.toString(edition);

        return "" +
                "Название: " + '\"' + name + '\"' +
                ", автор: " + author +
                ", издание: " + editionValue +
                ", стоимость: " + price + " гривен";
    }
}
