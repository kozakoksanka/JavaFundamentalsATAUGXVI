package ua.org.oa.kozakoksanka;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Test class.
 *
 * @author Oksana Zaika.
 */
public class LibraryTest {
    /**
     * Application entry point method.
     *
     * @param args Console arguments.
     */
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


        // Create list.
        List<Book> allBooks = new ArrayList<>();

        // Add objects into the list.
        allBooks.add(new Book("Война и мир", "Л.Толстой", 200));
        allBooks.add(new Book("Евгений Онегин", "А.Пушкин", 150));
        allBooks.add(new Book("Кобзар", "Т.Шевченко", 270));
        allBooks.add(new Book("Мастер и Маргарита", "М.Булгаков", 325, 8));
        allBooks.add(new Book("Идиот", "Ф.Достоевский", 300));
        allBooks.add(new Book("Герой нашего времени", "М.Лермонтов", 315));
        allBooks.add(new Book("Тарас Бульба", "Н.Гоголь", 175));
        allBooks.add(new Book("Человек, который смеется", "В.Гюго", 120, 5));
        allBooks.add(new Book("Портрет Дориана Грея", "О.Уальд", 540, 7));

        // Add objects into the list with console.

        String choice;
        do {
            System.out.println("Напишите название книги:");
            String name = br.readLine();
            System.out.println("Напишите автора книги:");
            String author = br.readLine();
            System.out.println("Укажите стоимость книги:");
            int price = Integer.parseInt(br.readLine());
            System.out.println("Укажите издание книги:");
            int edition = Integer.parseInt(br.readLine());
            Book book = new Book(name, author, price, edition);
            allBooks.add(book);
            System.out.println("Вы хотите добавить еще одну книгу? (Да или Нет)");
            choice = br.readLine();
        } while (choice.equals("Да"));

        // Create library instance.
        Library library = new Library(allBooks);

        System.out.println("Книги, стоимость которых больше средней стоимости книг по данной библиотеке:");
        library.printOverAverageBook();

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        System.out.println("Книги, стоимость которых больше заданой суммы:");
        library.printOverPrice(230);

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        System.out.println("Книги, стоимость которых меньше средней стоимости книг по данной библиотеке:");
        library.printUnderAverageBook();

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        System.out.println("Книги, стоимость которых меньше заданой суммы:");
        library.printUnderPrice(230);

    }
}
