package ua.org.oa.kozakoksanka;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents Library which includes methods books manipulation.
 * @author Oksana Zaika;
 */
public class Library {
    /**
     * Create ArrayList allBook which consists with books.
     */
    private List<Book> allBooks = new ArrayList<>();

    /**
     * Constructor
     * @param allBooks list of all books.
     */
    public Library(List<Book> allBooks) {
        this.allBooks = allBooks;
    }

    /**
     * Create a method that calculate the price is above average in this library.
     * @return average price value.
     */

    private long getAveragePrice() {
        long sum = 0;
        for (Book book : allBooks) {
            sum += book.getPrice();
        }
        return sum / allBooks.size();
    }

    /**
     * Create a method that print all books which cost over average price of all books in this library.
     */
    public void printOverAverageBook() {
        long averagePrice = getAveragePrice();
        for (Book book : allBooks) {
            if (book.getPrice() > averagePrice) {
                System.out.println(book.toString());
            }
        }
    }

    /**
     * Create a method which print all books which cost over some price.
     * @param price value of books price.
     */
    public void printOverPrice(int price) {
        for (Book book : allBooks) {
            if (book.getPrice() > price) {
                System.out.println(book.toString());
            }
        }
    }

    /**
     * Create a method that print all books which cost under average price of all books in this library.
     */
    public void printUnderAverageBook() {
        long averagePrice = getAveragePrice();
        for (Book book : allBooks) {
            if (book.getPrice() < averagePrice) {
                System.out.println(book.toString());
            }
        }
    }

    /**
     * Create a method which print all books which cost under some price.
     * @param price value of books price.
     */
    public void printUnderPrice(int price) {
        for (Book book : allBooks) {
            if (book.getPrice() < price) {
                System.out.println(book.toString());
            }
        }
    }
}


