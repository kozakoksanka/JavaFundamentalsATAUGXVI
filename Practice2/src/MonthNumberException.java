/**
 * Represents exception of months numeration.
 * @author Oksana Zaika.
 */
public class MonthNumberException extends Exception {
    public MonthNumberException (String message){
        super(message);
    }
}
