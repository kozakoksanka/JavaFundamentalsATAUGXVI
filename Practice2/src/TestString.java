import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Test class.
 *
 * @author Oksana Zaika.
 */
public class TestString {
    /**
     * Application entry point method.
     * @param args console argument.
     */
    public static void main(String args[]) {
        /**Test for task 1*/
        System.out.println("~~~~~Test for task 1~~~~~");
        System.out.println(StringUtils.concatStrings("Sweet", "Candy"));

        /**Test for task 2*/
        System.out.println("~~~~~Test for task 2~~~~~");
        System.out.println(StringUtils.takeTreeCenterLetter("Candy"));

        /**Test for task 3*/
        System.out.println("~~~~~Test for task 3~~~~~");
        System.out.println(StringUtils.createNewString("Candy"));

        /**Test for task 4*/
        System.out.println("~~~~~Test for task 4~~~~~");
        System.out.println(StringUtils.createStringWithDuplicateSymbols("Candy"));

        /**Test for task 5*/
        System.out.println("~~~~~Test for task 5~~~~~");
        StringUtils.findLetters("bob is bab bab bab file bob");

        /**Test for task 6*/
        System.out.println("~~~~~Test for task 6~~~~~");
        System.out.println(StringUtils.removeSymbols("S*uun*ny s*um*mer"));

        /**Test for task 7*/
        System.out.println("~~~~~Test for task 7~~~~~");
        StringUtils.calculateAmountOfWords("Candy Candys CandyS CanadaS, Canada");

        /**Test for task 8*/
        System.out.println("~~~~~Test for task 8~~~~~");
        System.out.println(StringUtils.createStringWithoutSubstring("summerSunnysummer", "summer"));

        /**
         * Test for last task.
         */
        System.out.println("~~~~~Test for last task~~~~~");
        WeatherAnalytics calendar = new WeatherAnalytics();
        calendar.analiseWeather();
    }
}


