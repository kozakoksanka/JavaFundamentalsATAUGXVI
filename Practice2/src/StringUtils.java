/**
 * Represents tasks 1-8 for practice lesson #2.
 *
 * @author Oksana Zaika.
 */
public class StringUtils {
    /**
     * Task 1.
     * Concatenates two substrings.
     *
     * @param a String first.
     * @param b String second.
     * @return result of concatenation two substrings.
     */
    public static String concatStrings(String a, String b) {
        return a.substring(1).concat(b.substring(1));
    }

    /**
     * Task 2.
     * Creates a new string from middle symbols of arguments string.
     *
     * @param s Any string.
     * @return new string.
     */
    public static String takeTreeCenterLetter(String s) {
        String result = s;
        int length = s.length();
        if (length % 2 != 0 && length >= 3) {
            int center = length / 2;
            result = s.substring(center - 1, center + 2);
        }
        return result;
    }

    /**
     * Task 3.
     * Creates a new string.
     *
     * @param s Any string.
     * @return new string.
     */
    public static String createNewString(String s) {
        int lengthS = s.length();
        String s1 = "" + s.charAt(lengthS - 2) + s.charAt(lengthS - 1) + s;
        return s1;
    }

    /**
     * Task 4.
     * Creates a new string with duplicate symbols.
     *
     * @param s Any string.
     * @return new string with duplicate symbols.
     */
    public static String createStringWithDuplicateSymbols(String s) {
        String greeting = "";
        for (int i = 0; i < s.length(); i++) {
            greeting = greeting + s.charAt(i) + s.charAt(i);
        }
        return greeting;
    }

    /**
     * Task 5.
     * Checks string and calculates amount of entry letters b*b.
     *
     * @param string Any string.
     */
    public static void findLetters(String string) {
        int count = 0;
        int begin = 0;
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == ' ') {
                if ((string.charAt(begin) == 'b') && (string.charAt(begin + 2) == 'b'))
                    count++;
                begin = i + 1;
            }
        }
        if ((string.charAt(string.length() - 1) == 'b') && (string.charAt(string.length() - 3) == 'b') && (string.charAt(string.length() - 4) == ' '))
            count++;
        System.out.println("bob is bab bab bab > " + count);
    }

    /**
     * Task 6.
     * Removes symbols in string.
     *
     * @param s Any string.
     * @return string without some symbols.
     */
    public static String removeSymbols(String s) {
        StringBuilder sb = new StringBuilder(s);
        int index = 0;
        while ((index = sb.indexOf("*", index)) != -1) {
            if (index != 0 && index != sb.length() - 1 && sb.charAt(index + 1) != '*') {
                sb.delete(index - 1, index + 2);
                index--;
            } else if (index == 0) {
                sb.delete(index, index + 2);
            } else if (index == sb.length() - 1) {
                sb.delete(index - 1, index + 1);
                index--;
            } else {
                sb.deleteCharAt(index);
            }
        }
        return sb.toString();
    }

    /**
     * Task 7.
     * Calculates amount of words ending with letters "a" or "s".
     * @param s Any string.
     */
    public static void calculateAmountOfWords(String s) {
        int amountWordsWithA = 0;
        int amountWordsWithS = 0;
        String[] test = s.trim().split("[,;:.!?\\s]+");

        for (String word : test) {
            if (word.charAt(word.length() - 1) == 'a' || word.charAt(word.length() - 1) == 'A') {
                amountWordsWithA++;
            } else if (word.charAt(word.length() - 1) == 's' || word.charAt(word.length() - 1) == 'S') {
                amountWordsWithS++;
            }
        }
        System.out.println("Amount of words ending with 'a' or 'A': " + amountWordsWithA);
        System.out.println("Amount of words ending with 's' or 'S': " + amountWordsWithS);
    }

    /**
     * Task 8.
     * Creates new string which includes first string without substring second.
     * @param first Any string.
     * @param second Any string.
     * @return new string which includes first string without substring second.
     */
    public static String createStringWithoutSubstring (String first, String second){
        String string3 = first.replaceAll(second, "");
        return string3;
    }
}






