import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Represents a program which analytics a temperature during a month.
 *
 * @author Oksana Zaika.
 */
public class WeatherAnalytics {
    /**
     * Minimal value of temperature.
     */
    private final int MIN_TEM = -50;

    /**
     * Maximal value of temperature.
     */
    private final int MAX_TEM = 50;

    /**
     * Number of the first month in the year.
     */
    private final int FIRST_MONTH = 1;
    /**
     * Number of the last month in the year.
     */
    private final int LAST_MONTH = 12;

    /**
     * Number of month.
     */
    private int month;

    /**
     * Amount of days in month.
     */
    private int amountOfDays;

    /**
     * Value of temperature.
     */
    private int temperature;

    // Creates list.
    ArrayList<Integer> list = new ArrayList<>(amountOfDays);

    // Creates BufferedReader.
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    /**
     * Gets number of month.
     * @return number of month.
     */
    public int getMonth() {
        return month;
    }

    /**
     * Sets number of month.
     * @param month number of month.
     */
    public void setMonth(int month) throws MonthNumberException {
        if (month >= FIRST_MONTH && month <= LAST_MONTH) {
            this.month = month;
        } else {
            throw new MonthNumberException("Wrong number of month, use numbers from " +
                    FIRST_MONTH + " to " + LAST_MONTH);
        }
    }
    /**
     * Gets value of temperature for day.
     * @return value of temperature for day.
     */
    public int getTemperature() {
        return temperature;
    }

    /**
     * Sets value of temperature for day.
     * @param temperature value of temperature.
     * @throws TemperatureException in case invalid temperature.
     */
    public void setTemperature(int temperature) throws TemperatureException {
        if (temperature > MIN_TEM && temperature < MAX_TEM) {
            this.temperature = temperature;
        } else {
            throw new TemperatureException("Wrong temperature, use temperature from "
                    + MAX_TEM + " to " + MAX_TEM + ":");
        }
    }

    /**
     * Obtains number of month by reading from console.
     * @param br BufferedReader.
     * @return number of month.
     */
    private int obtainNumberOfMonth(BufferedReader br){
        try {
            while (true) {
                try {
                    setMonth(Integer.parseInt(br.readLine()));
                        break;
                } catch (NumberFormatException e) {
                    System.out.println("Wrong number try again: ");
                } catch (MonthNumberException e) {
                    System.out.println(e.getMessage());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return month;
    }

    /**
     * Obtains amount of days in month by number of month.
     * @return amount of days in each month.
     */
    private int obtainAmountOfDaysInMonth() {
        System.out.println("Add number of month: ");
        month = obtainNumberOfMonth(br);
        switch (month) {
            case 1:
                amountOfDays = 31;
                break;
            case 2:
                amountOfDays = 3;
                break;
            case 3:
                amountOfDays = 31;
                break;
            case 4:
                amountOfDays = 30;
                break;
            case 5:
                amountOfDays = 31;
                break;
            case 6:
                amountOfDays = 30;
                break;
            case 7:
                amountOfDays = 31;
                break;
            case 8:
                amountOfDays = 31;
                break;
            case 9:
                amountOfDays = 30;
                break;
            case 10:
                amountOfDays = 31;
                break;
            case 11:
                amountOfDays = 30;
                break;
            case 12:
                amountOfDays = 31;
                break;
            default:
                System.out.println("Year gets 12 month only!");
        }
        System.out.println("The month consists of " + amountOfDays + " days.");
        return amountOfDays;
    }

    /**
     * Obtains value of temperature for each day of month by reading from console.
     * @param br BufferedReader.
     * @return value of temperature.
     */
    private int obtainTemperatureForEachDay(BufferedReader br) {
        int num;
        try {
            while (true) {
                try {
                    num = Integer.parseInt(br.readLine());
                    setTemperature(num);
                    break;
                } catch (NumberFormatException e) {
                    System.out.println("Wrong number try again");
                } catch (TemperatureException t) {
                    t.getMessage();
                    System.out.println(t.getMessage());
                }
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
        return temperature;
    }

    /**
     * Fills list temperature for each day and prints temperature for all days of month.
     */
    private void fillList() {
        amountOfDays = obtainAmountOfDaysInMonth();
        for (int i = 0; i < amountOfDays; i++) {
            System.out.println("Enter temperature in Celsius by " + (i + 1) + " day.");
            list.add(i, temperature = obtainTemperatureForEachDay(br));
        }
        System.out.println("In " + month + " month were the following temperatures: "
                + list.toString());
    }

    /**
     * Calculates average temperature during month and prints it.
     */
    private void calculateAverageTemperature() {
        int averageTemperature = 0;
        int sum = 0;
        for (int i = 0; i < list.size(); i++) {
            sum += list.get(i);
        }
        averageTemperature = sum / list.size();
        System.out.println("Average temperature for " + month + " month was: "
                + averageTemperature + " degrees.");
    }

    /**
     * Calculates maximal and minimal temperatures during month and prints it.
     */
    private void calculateMaxAndMinTemperature() {
        int maxTemperature = list.get(0);
        int dayMax = 0;
        int minTemperature = list.get(0);
        int dayMin = 0;
        for (int i = 1; i < list.size(); i++) {
            if (maxTemperature < list.get(i)) {
                maxTemperature = list.get(i);
                dayMax = i;
            }
            if (minTemperature > list.get(i)) {
                minTemperature = list.get(i);
                dayMin = i;
            }
        }
        System.out.println("The warmest day of " + month + " month was "  + (dayMax + 1)
                + " and temperature was: " + maxTemperature + " degrees.");
        System.out.println("The coldest day of " + month + "month was " + (dayMin + 1)
                + " and temperature was: " + minTemperature + " degrees.");
    }

    /**
     * Analyses weather.
     */
    public void analiseWeather() {
        fillList();
        calculateAverageTemperature();
        calculateMaxAndMinTemperature();
    }

}







