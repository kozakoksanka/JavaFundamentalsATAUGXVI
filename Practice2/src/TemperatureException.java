/**
 * Represents temperature exception.
 *
 * @author Oksana Zaika.
 */
public class TemperatureException extends Exception {
    public TemperatureException(String message) {
        super(message);
    }
}
