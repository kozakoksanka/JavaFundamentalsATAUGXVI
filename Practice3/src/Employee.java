/**
 * Represents a simple model of employee.
 *
 * @author Oksana Zaika
 */
abstract class Employee {

    /**
     * Surname of employee.
     */
    private String surname;

    /**
     * Name of employee.
     */
    private String name;

    /**
     * Post of employee.
     */
    private String post;

    /**
     * Constructor.
     *
     * @param surname Surname of employee.
     * @param name    Name of employee.
     * @param post    Post of employee.
     */
    public Employee(String surname, String name, String post) {
        setSurname(surname);
        setName(name);
        setPost(post);
    }

    /**
     * @return surname of employee.
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets surname of employee.
     *
     * @param surname Surname of employee.
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * @return name of employee.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name of employee.
     *
     * @param name Name of employee.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return post of employee.
     */
    public String getPost() {
        return post;
    }

    /**
     * Sets post of employee.
     *
     * @param post Post of employee.
     */
    public void setPost(String post) {
        this.post = post;
    }

    /**
     * Create abstract method which calculate a salary of employee.
     *
     * @return value of employee's salary.
     */
    abstract int calculateSalary();

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return getClass().getSimpleName() +
                " - " + post + ": " + name + " " +
                surname + ", месячная зарплата: " + calculateSalary() + " гривен.";
    }
}
