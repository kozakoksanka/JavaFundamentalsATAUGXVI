/**
 * Represents a simple model piece worker, which extended from Employee.
 *
 * @author Oksana Zaika.
 */
public class PieceWorker extends Employee {

    /**
     * Price of each one product.
     */
    public static final int PRICE_OF_ONE_PIECE = 30;

    /**
     * Amount of products in month.
     */
    private int amountOfPiecesOfMonth;

    /**
     * Constructor.
     *
     * @param surname Surname of employee.
     * @param name    Name of employee.
     * @param post    Post of employee.
     */
    public PieceWorker(String surname, String name, String post, int amountOfPiecesOfMonth) {
        super(surname, name, post);
        setAmountOfPiecesOfMonth(amountOfPiecesOfMonth);
    }

    /**
     * @return amount of products in month.
     */
    public int getAmountOfPiecesOfMonth() {
        return amountOfPiecesOfMonth;
    }

    /**
     * Sets amount of products in month.
     * @param amountOfPiecesOfMonth Amount of products in month.
     */

    public void setAmountOfPiecesOfMonth(int amountOfPiecesOfMonth) {
        this.amountOfPiecesOfMonth = amountOfPiecesOfMonth;
    }

    /**
     * Calculates piece worker's salary.
     *
     * @return value of piece worker's salary.
     */
    @Override
    int calculateSalary() {
       return amountOfPiecesOfMonth * PRICE_OF_ONE_PIECE;
    }
}
