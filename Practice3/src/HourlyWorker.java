/**
 * Represents a simple model hourly worker, which extended from Employee.
 *
 * @author Oksana Zaika.
 */

public class HourlyWorker extends Employee {

    /**
     * Salary's value of hour.
     */
    public static final int SALARY_OF_HOUR = 100;

    /**
     * Work rate in month (in hours).
     */
    public static final int RATE_OF_MONTH = 160;

    /**
     * Quantity of hours in current month.
     */
    private int workHours;

    /**
     * Value of coefficient in cases of extra hours work.
     */
    public static final double COEFFICIENT = 1.1;

    /**
     * Constructor.
     *
     * @param surname Surname of employee.
     * @param name    Name of employee.
     * @param post    Post of employee.
     */
    public HourlyWorker(String surname, String name, String post, int workHours) {
        super(surname, name, post);
        setWorkHours(workHours);
    }

    /**
     * @return value of working hours in current month.
     */
    public int getWorkHours() {
        return workHours;
    }

    /**
     * Sets value of working hours in current month.
     *
     * @param workHours Value of working hours in current month.
     */
    public void setWorkHours(int workHours) {
        this.workHours = workHours;
    }

    /**
     * Calculates hourly worker's salary.
     *
     * @return value of hourly worker's salary.
     */
    @Override
    int calculateSalary() {
        double result = 0;
        if (workHours <= RATE_OF_MONTH) {
            result = workHours * SALARY_OF_HOUR;
        } else if (workHours > RATE_OF_MONTH) {
            result = ((workHours - RATE_OF_MONTH) * SALARY_OF_HOUR * COEFFICIENT) + (RATE_OF_MONTH * SALARY_OF_HOUR);
        }
        return (int) result;
    }
}
