import java.util.ArrayList;

/**
 * Test class.
 * @author Oksana Zaika.
 *
 */
public class SalaryTest {

    /**
     * Application entry point method.
     *
     * @param args Console arguments.
     */
    public static void main(String[] args) {

        // Create object Boss.
        Boss bigBoss = new Boss("Пустовой", "Андрей", "директор");

        // Create object HourlyWorker.
        HourlyWorker engineer = new HourlyWorker("Заводов", "Василий", "инженер", 180);

        // Create object PieceWorker.
        PieceWorker pieceWorker = new PieceWorker("Малий", "Ольга", "кондитер", 200);

        // Create object CommissionWorker.
        CommissionWorker saler = new CommissionWorker("Ищик", "Вадим", "продавец", 20000);

        // Create array list.
        ArrayList<Employee> sweetCompany = new ArrayList<Employee>();
        sweetCompany.add(bigBoss);
        sweetCompany.add(engineer);
        sweetCompany.add(pieceWorker);
        sweetCompany.add(saler);
        sweetCompany.add(new HourlyWorker("Чала", "Марина", "технолог", 160));
        System.out.println(sweetCompany.toString());

        // Calling static method which calculates and prints salary sum for all company.
        Company.calculateAndPrintSumOfSalary(sweetCompany);
    }
        }

