/**
 * Represents a simple model of boss, which extended from Employee.
 *
 * @author Oksana Zaika.
  */
public class Boss extends Employee {

    /**
     * Constant which includes value of boss's weekly pay.
     */
    private static final int BOSS_WEEKLY_PAY = 5000;

    /**
     * @return value of boss's weekly pay.
     */
    public static int getBossWeeklyPay() {
        return BOSS_WEEKLY_PAY;
    }

    /**
     * Constructor.
     *
     * @param surname Surname of boss.
     * @param name    Name of boss.
     * @param post    Post of boss.
     */
    public Boss(String surname, String name, String post) {
        super(surname, name, post);
    }

    /**
     * Calculates boss's salary.
     *
     * @return value of boos's salary.
     */
    @Override
    int calculateSalary() {
        return getBossWeeklyPay() * 4;
    }
}

