/**
 * Represents a simple model commission worker, which extended from Employee.
 * @author Oksana Zaika.
 */
public class CommissionWorker extends Employee {

    /**
     * Value of base rate.
     */
    public static final int BASE_RATE = 3000;

    /**
     * Value ol percent from sale.
     */
    public static final double PERCENT_FROM_SALE = 0.15;

    /**
     * Amount of sales.
     */
    private int amountOfSales;

    /**
     * Constructor.
     *
     * @param surname Surname of employee.
     * @param name    Name of employee.
     * @param post    Post of employee.
     */
    public CommissionWorker(String surname, String name, String post, int amountOfSales) {
        super(surname, name, post);
        setAmountOfSales(amountOfSales);
    }

    /**
     * @return
     */
    public int getAmountOfSales() {
        return amountOfSales;
    }

    /**
     * Sets
     * @param amountOfSales
     */
    public void setAmountOfSales(int amountOfSales) {
        this.amountOfSales = amountOfSales;
    }


    /**
     * Calculates commission worker's salary.
     * @return value of commission worker's salary.
     */
    @Override
    int calculateSalary() {
        double result = BASE_RATE + (amountOfSales * PERCENT_FROM_SALE);
        return (int) result;
    }
}
