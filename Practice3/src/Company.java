import java.util.ArrayList;

/**
 * Includes method which calculates and prints salary sum for all company.
 *
 * @author Oksana Zaika.
 */
public class Company {

    /**
     * Calculates and prints salary sum for all company.
     *
     * @param list array list;
     */
    public static void calculateAndPrintSumOfSalary(ArrayList<Employee> list) {
        int sum = 0;
        for (Employee worker : list) {
            sum += worker.calculateSalary();
        }
        System.out.println("Сумма зароботных плат всех сотрудников компании в месяц составляет: " + sum + " гривен.");
    }
}
