/**
 * Test class.
 *
 * @author Oksana Zaika.
 */
public class TestClass {
    public static void main(String[] args) {
        DynamicStringList list = new DynamicStringList();
        System.out.println("-----------------Добавить элемент:-----------------");
        list.add("orange");
        list.add("apple");
        System.out.println("-----------------Взять последний элемент массива:-----------------");
        System.out.println(list.get());
        System.out.println("-----------------Взять элемент массива по индексу:-----------------");
        System.out.println(list.get(1));
        System.out.println("-----------------Удалить последний элемент массива:-----------------");
        System.out.println(list.remove());
        System.out.println(list);
        list.add("cucumber");
        list.add("garlic");
        list.add("potato");
        System.out.println(list);
        System.out.println("-----------------Удалить элемент массива по индексу:-----------------");
        System.out.println(list.remove(2));
        System.out.println(list);
        System.out.println("-----------------Удалить весь массив:-----------------");
        System.out.println(list.delete());
        System.out.println(list);
    }
}

