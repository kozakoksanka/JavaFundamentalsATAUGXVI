import java.util.Arrays;

/**
 * Represents array implementation of SimpleList interface.
 *
 * @author Oksana Zaika.
 */
public class DynamicStringList implements SimpleList {

    /**
     * Minimal value of array size.
     */
    private static final int MIN_ARRAY_SIZE = 0;

    /**
     * Counter of elements in array.
     */
    private int counterOfElements;

    /**
     * Array of elements.
     */
    private String[] elements;

    /**
     * Empty constructor without arguments.
     */
    public DynamicStringList() {
        this(MIN_ARRAY_SIZE);
    }

    /**
     * Constructor.
     *
     * @param size value of array's minimal size.
     */
    public DynamicStringList(int size) {
        elements = new String[size];
    }

    /**
     * Adds string at the start of array.
     *
     * @param s Any string.
     */
    @Override
    public void add(String s) {
        if (counterOfElements == elements.length) {
            createBiggerArray();
        }
        elements[counterOfElements++] = s;
        System.out.println(Arrays.toString(elements));
    }

    /**
     * Multiplication current array by 2.
     */
    private void createBiggerArray() {
        elements = Arrays.copyOf(elements, elements.length + 1);
    }

    /**
     * Get last element of array.
     *
     * @return last element of array.
     */
    @Override
    public String get() {
        if (elements.length == 0) {
            throw new RuntimeException("List is empty");
        }
        return elements[elements.length - 1];
    }

    /**
     * Check location of amountOfElements in array.
     *
     * @param index Index of array's element.
     */
    private boolean isIndexInUse(int index) {
        if (index < 0 && index >= elements.length - 1) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get array's element by it's number in array.
     *
     * @param id Number for get.
     * @return value of array's element.
     */
    @Override
    public String get(int id) {
        String result = null;
        if (isIndexInUse(id)) {
            result = elements[id];
        }
        return result;
    }

    /**
     * Delete last array's element and creates smallest array.
     *
     * @return element with was deleted.
     */
    @Override
    public String remove() {
        if (counterOfElements == 0) {
            throw new RuntimeException("Array is empty");
        }
        String result = elements[elements.length - 1];
        elements = Arrays.copyOf(elements, elements.length - 1);
        counterOfElements--;
        return result;
    }

    /**
     * Delete array's element by its id number and creates smallest array.
     *
     * @param id Number for delete.
     * @return element with was deleted.
     */
    @Override
    public String remove(int id) {
        String result = null;
        int newIndex = 0;
        String[] arrayAfterDelete = new String[elements.length - 1];

        if (isIndexInUse(id)) {
            for (int i = 0; i < elements.length; i++) {
                if (i != id) {
                    arrayAfterDelete[newIndex] = elements[i];
                    newIndex++;
                } else {
                }
            }
            result = get(id);
        }
        System.out.println(counterOfElements);
        elements = arrayAfterDelete;
        return result;
    }

    /**
     * Delete all elements of array and makes its size default.
     *
     * @return false if array is already empty and true if elements of array were deleted.
     */
    @Override
    public boolean delete() {
        if (counterOfElements == 0) {
            return false;
        } else {
            counterOfElements = 0;
            elements = new String[MIN_ARRAY_SIZE];
            System.out.println(counterOfElements);
            return true;
        }

    }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString () {
            return "DynamicStringList consists of elements: "
                    + Arrays.toString(elements);
        }
    }


