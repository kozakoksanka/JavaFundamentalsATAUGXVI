/**
 * Represents methods of interface.
 *
 * @author Oksana Zaika.
 */
public interface SimpleList  {

    /**
     * Adds string at the start of array.
     */
    void add(String s);

    /**
     * Gets the last element of array.
     * @return value of last element of array.
     */
    String get();

    /**
     * Gets element by its index.
     * @param id Number for get.
     * @return value of element by its index.
     */
    String get(int id);

    /**
     * Deletes the last element of array.
     * @return value of last element of array which was deleted.
     */
    String remove();

    /**
     * Deletes element of array by its index.
     * @param id Number for delete.
     * @return value of last element of array which was deleted..
     */
    String remove(int id);

    /**
     * Delete all elements of array and makes its size default.
     * @return false if array is already empty and
     * true if elements of array were deleted.
     */
    boolean delete();
}
