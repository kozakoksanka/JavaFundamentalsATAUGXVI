/**
 * @author Oksana Zaika.
 */
public class SimpleDataTypesTest {

    public static void main(String[] arguments) {
        // Task 1.
        // Fields.
        System.out.println("----------First Test----------");
        System.out.println(SimpleDataTypesUtils.myByte);
        System.out.println(SimpleDataTypesUtils.myShort);
        System.out.println(SimpleDataTypesUtils.myInt);
        System.out.println(SimpleDataTypesUtils.myLong);
        System.out.println(SimpleDataTypesUtils.myFloat);
        System.out.println(SimpleDataTypesUtils.myDouble);
        System.out.println(SimpleDataTypesUtils.myChar);
        System.out.println(SimpleDataTypesUtils.myBoolean);

        // Local variables.
        byte myByte1 = 1;
        short myShort1 = 2;
        int myInt1 = 3;
        long myLong1 = 4;
        float myFloat1 = 5;
        double myDouble1 = 6;
        char myChar1 = 7;
        System.out.println(myByte1);
        System.out.println(myShort1);
        System.out.println(myInt1);
        System.out.println(myLong1);
        System.out.println(myFloat1);
        System.out.println(myDouble1);
        System.out.println(myChar1);


        // Task 2.
        System.out.println("----------Second Test----------");
        float f1 = 1.f;
        float f2 = 1;
        float f3 = 0x1;
        float f4 = 0b1;
        float f5 = 1.0e1f;
        float f6 = 01;
        System.out.println(f1);
        System.out.println(f2);
        System.out.println(f3);
        System.out.println(f4);
        System.out.println(f5);
        System.out.println(f6);

        // Task 3.
        System.out.println("----------Third Test----------");

        short short1 = 2 + 2;
        short short2 = (short) (myFloat1 + myInt1);
        short short3 = (short) (2 + 2.5f);
        short short4 = (short) (myByte1 + myShort1);
        short short5 = (short) (myFloat1 + myDouble1);
        System.out.println(short1);
        System.out.println(short2);
        System.out.println(short3);
        System.out.println(short4);
        System.out.println(short5);


        // Task 4.
        System.out.println("----------Fourth Test----------");
        System.out.println(SimpleDataTypesUtils.getSquare(5, 3, 4));

        // Task 5.
        System.out.println("----------Fifth Test----------");
        System.out.println(SimpleDataTypesUtils.calculatesSumOfNumbersInDiapasone(1, 20));

        // Task 6.
        System.out.println("----------Sixth Test----------");
        System.out.println(SimpleDataTypesUtils.calculatesSumOfEvenNumbers(1,20));

        // Task 7.
        System.out.println("----------Seventh Test----------");
        System.out.println(SimpleDataTypesUtils.calculateSumOfSimpleNum(1,20));

        // Task 8.
        System.out.println("----------Eight Test----------");
        System.out.println(SimpleDataTypesUtils.getSumOfABC(5,9,14));

        // Task 9.
        System.out.println("----------Ninth Test----------");
        System.out.println(SimpleDataTypesUtils.getMiddleOfAB(50, 5));

        // Task 10.
        System.out.println("----------Tenth Test----------");
        SimpleDataTypesUtils.getInformationAboutCredit(10000, 10, 20);
    }
}
