/**
 * Represents methods to first practical training.
 *
 * @author Oksana Zaika.
 */
public class SimpleDataTypesUtils {

    /**
     * Task 1.
     */
    static byte myByte;
    static short myShort;
    static int myInt;
    static long myLong;
    static float myFloat;
    static double myDouble;
    static char myChar;
    static boolean myBoolean;

    /**
     * Task 4.
     * Checks rectangular triangle or not.
     * @param hypotenuse Value of hypotenuse.
     * @param cathetus1  Value of cathetus.
     * @param cathetus2  Value of cathetus.
     * @return result of check: rectangular or not rectangular.
     */
    public static String getSquare(int hypotenuse, int cathetus1, int cathetus2) {

        return (cathetus1 * cathetus1 + cathetus2 * cathetus2 == hypotenuse * hypotenuse) ? "Rectangular" : "Not rectangular";
    }

    /**
     * Task 5.
     * Calculates sum of numbers in diapason.
     * @param first Start number of diapason.
     * @param end   Last number of diapason.
     * @return sum of numbers in diapason.
     */
    public static String calculatesSumOfNumbersInDiapasone(int first, int end) {
        int sum = 0;
        for (int i = first; i <= end; i++) {
            sum = sum + i;
        }
        return "Sum of numbers from " + first + " to " + end + ": " + sum;
    }

    /**
     * Task 6.
     * Calculates sum of even numbers in diapason.
     * @param first Start number of diapason.
     * @param end   Last number of diapason.
     * @return sum of even numbers.
     */
    public static String calculatesSumOfEvenNumbers(int first, int end) {
        int sum = 0;
        for (int i = first; i <= end; i++) {
            if (i % 2 == 0) {
                sum = sum + i;
            }
        }
        return "Sum of even numbers: " + sum;
    }

    /**
     * Task 7.
     * Calculates sum of simple numbers in diapason.
     * @param first Start number of diapason.
     * @param end   Last number of diapason.
     * @return sum of simple numbers.
     */
    public static String calculateSumOfSimpleNum(int first, int end) {
        int sum = 0;
        m:
        for (int i = first; i <= end; i++) {
            for (int n = 2; n < i; n++) {
                if (i % n == 0) {
                    continue m;
                }
            }
            if (i != 1)
                sum = sum + i;
        }
        return "Sum : " + sum;
    }

    /**
     * Task 8.
     * Returns true if sum of any two numbers is a value of third number.
     * @param a First number.
     * @param b Second number.
     * @param c Third number.
     * @return true if sum of any two numbers is a value of third number.
     */
    public static String getSumOfABC(int a, int b, int c) {
        return ((a == b + c) || (b == a + c) || (c == b + a)) ? "true" : "false";
    }

    /**
     * Task 9.
     * Calculates a middle value of numbers in diapason.
     * @param a Start number of diapason.
     * @param b Last number of diapason.
     * @return middle value of numbers in diapason.
     */
    public static double getMiddleOfAB(int a, int b) {
        double result = 0;
        int sum = 0;
        int quantity = 0;
        if ((a > 0 && a > b) && (b > 0)) {
            for (int i = a; i > b; i--) {
                sum = sum + i;
                ++quantity;
            }
        } else {
            System.out.println("Wrong value of arguments.");
        }
        if (sum != 0 && quantity != 0) {
            result = sum / quantity;
        }
        return result;
    }

    /**
     * Task 10.
     * Calculates percent payment of each month of credit.
     * @param sumOfCredit       Total sum of credit.
     * @param amountOfMonth     Amount of month of credit.
     * @param percentRateOfYear Percent rate of credit.
     */
    public static void getInformationAboutCredit(int sumOfCredit, int amountOfMonth, int percentRateOfYear) {
        int bodyPaymentPerMonth = sumOfCredit / amountOfMonth;
        int body = sumOfCredit;
        int percentsSum = 0;
        int counterOfMonth = 0;
        for (int i = 0; i < amountOfMonth; i++) {
            int percentsPayment = ((body * percentRateOfYear) / 100) / 12;
            percentsSum += percentsPayment;
            body = body - bodyPaymentPerMonth;
            counterOfMonth++;
            System.out.println("Percents payment for " + counterOfMonth + " month is: " + percentsPayment +
                    ", body payment: " + bodyPaymentPerMonth + ";");
        }
        System.out.println("Total percent payment of " + amountOfMonth + " month is: " + percentsSum + ".");
    }
}
