/**
 * Test class.
 *
 * @author Oksana Zaika.
 */
public class TestClass {
    /**
     * Application entry point method
     *
     * @param args Console arguments.
     */
    public static void main(String[] args){
        // Creates object Group and initializes size of array.
        Group myGroup = new Group(2);

        // Adds students to the group.
        myGroup.addStudentToGroup();

        // Print all students of group.
        System.out.println(myGroup.toString());
    }
}
