/**
 * Represents age exception.
 * @author Oksana Zaika.
 */
public class AgeException extends StudentsException {
    /**
     * Calling in case of age exception.
     * @param message any message.
     */
    public AgeException (String message){
        super(message);
    }
}
