/**
 * Represents age exception.
 * @author Oksana Zaika.
 */
public class NameException extends StudentsException {
    /**
     * Calling in case of name exception.
     * @param message any message.
     */
    public NameException (String message){
        super(message);
    }
}
