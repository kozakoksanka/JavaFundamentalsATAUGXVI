/**
 * Created by oksana on 10.10.16.
 */
public class StudentsException extends Exception {
    public StudentsException(String message) {
        super(message);
    }
}

