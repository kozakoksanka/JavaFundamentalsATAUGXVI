import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Represents a simple model of students group.
 *
 * @author Oksana Zaika.
 */
public class Group extends Student {
    /**
     * Array of students.
     */
    private Student[] group;

    /**
     * Constructor.
     *
     * @param sizeOfGroup size of array.
     */
    public Group(int sizeOfGroup) {
        group = new Student[sizeOfGroup];
    }

    /**
     * Creates a student from console.
     *
     * @return student.
     */
    private Student createStudentConsoleValidation() {
        Student student = new Student();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new UncloseInputSream(System.in)))) {
            while (true) {
                try {
                    student.setName(readString(br, "Введите имя и фамилию студента: "));
                    break;
                } catch (NameException e) {
                    System.out.println(e.getMessage());
                }
            }
            while (true) {
                try {
                    student.setAge(readNumber(br, "Введите возраст студента :"));
                    break;
                } catch (AgeException e) {
                    System.out.println(e.getMessage());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return student;
    }

    /**
     * Reads strings from console.
     *
     * @param br BufferedReader.
     * @param s  String.
     * @return string.
     * @throws IOException in case of wrong input or output.
     * @throws NameException in case invalid name.
     */
    private String readString(BufferedReader br, String s) throws IOException, NameException {
        String string = "";
        try {
            System.out.println(s);
            string = br.readLine();
            if (string.matches("(?i).*[0-9].*")) throw new NameException("Неправильный формат имени и фамилии," +
                    " используйте буквы.");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return string;
    }

    /**
     * Reads number from console.
     *
     * @param br BufferedReader.
     * @param s  String.
     * @return number.
     */
    private int readNumber(BufferedReader br, String s) {
        int number = 0;
        try {
            while (true) {
                try {
                    System.out.println(s);
                    number = Integer.parseInt(br.readLine());
                    break;
                } catch (NumberFormatException e) {
                    System.out.println("Возраст заполняется числами, а не символами.");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return number;
    }

    /**
     * Adds students to the array of group.
     */
    public void addStudentToGroup() {
        Student student;
        for (int i = 0; i < group.length; i++) {
            student = createStudentConsoleValidation();
            group[i] = student;
        }
        System.out.println(Arrays.toString(group));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Группа состоит из " + group.length + " студентов: " + " "
                + Arrays.toString(group);
    }
}


