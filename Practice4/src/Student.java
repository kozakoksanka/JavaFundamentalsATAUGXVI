/**
 * Represents a simple model of student.
 *
 * @author Oksana Zaika
 */
public class Student {
    /**
     * Value of name's minimal length.
     */
    static final int MIN_NAME_LENGTH = 2;

    /**
     * Minimal value of age.
     */
    private static final int MIN_AGE = 16;

    /**
     * Maximal value of age.
     */
    private static final int MAX_AGE = 60;

    /**
     * Name of student.
     */
    private String name;

    /**
     * Age of student;
     */
    private int age;

    /**
     * Constructor.
     */
    public Student() {}

    /**
     * Constructor.
     *
     * @param name name of student.
     * @param age age of student.
     * @throws NameException in case of invalid name.
     * @throws AgeException in case of invalid age.
     */
    public Student(String name, int age) throws NameException, AgeException {
        setName(name);
        setAge(age);
    }

    /**
     * Gets name of student.
     *
     * @return name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets student's name.
     *
     * @param name name.
     * @throws NameException in case of invalid name.
     */
    public void setName(String name) throws NameException {
        if (name.length() >= MIN_NAME_LENGTH) {
            this.name = name;
        } else {
            throw new NameException("Wrong name, use name length more then " + MIN_NAME_LENGTH);
        }
    }

    /**
     * Gets age of student.
     *
     * @return value of age.
     */
    public int getAge() {
        return age;
    }

    /**
     * Sets value of age.
     *
     * @param age
     * @throws AgeException in case of invalid age.
     */
    public void setAge(int age) throws AgeException {
        if (age >= MIN_AGE && age <= MAX_AGE) {
            this.age = age;
        } else {
            throw new AgeException("Возраст задан не верно, студентом можно быть с " + MIN_AGE + " до " + MAX_AGE
                    + " лет.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int q = 37;
        int p = 1;
        char charValueOfName = 0;
        for (int i = 0; i < getName().length(); i++) {
            charValueOfName += (getName().charAt(i));
        }
        int numberValueOfName = charValueOfName;
        int result = (p * numberValueOfName) + (q * age);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object object) {
        if (this == object)
            return true;
         if (object == null)
            return false;
        if (this.getClass() != object.getClass())
            return false;
        Student student = (Student) object;
        return name.equals(student.name) && age == student.age;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "cтудент " + getName() +
                ", возраст:" + getAge() +
                " лет";
    }
}

