import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Oksana Zaika.
 */
public class UncloseInputSream extends FilterInputStream {
    protected UncloseInputSream(InputStream in) {
        super(in);
    }

    @Override
    public void close() throws IOException {

    }
}